Shift Cipher 1 -- When in Rome
---

**Introduction**

A nice, simple challenge to get you warmed up.

**Challenge**

Decode the string below to get the flag.

`dtzltyrdkqfl`

---

A basic Caesar Cipher. Users are given the string `dtzltyrdkqfl` which is the string `yougotmyflag` shifted by 5 (we can play with different shifts).

If you have Python 3 installed, here is a function to do the shifts:

```
def caesar(plaintext, shift, alphabet="abcdefghijklmnopqrstuvwxyz"):
    shifted = alphabet[shift:] + alphabet[:shift]
    return plaintext.translate(str.maketrans(alphabet, shifted))
```