#!/usr/bin/env python3

from pyDes import des, PAD_PKCS5

key = "p@Ssw0rd"
text = """\
"";^[`#:><[\+>>&-^&|%])](&*?\]*">._]*~{`$|([&[%|.=<{./+-*~>=;`:*'_})]$?;&./>=[`{
;?@*)}`|}@^^?>.]'=$!:~%#``:__=|;[@@~-`=&#^[`^''?.*'?#!%#%./@+!{@^#?+-_,~,-?,)))*
</+[_~-*~)_":=/#~+%'.({{:\.`_,/)`,@{,)&&?//?*>^~%{($~/}._}*:"`'[)\\$&`>:%%])?,(_
`*\))\-};{"~*\~,.?#!**[~~<|*::^<#]`<+@?,-[#@:,$;&"<|]\][|:@&@)"-|-\,\=>}.{|;~$`*
_%[|?^}`(|]:)@},./,"]_\}>"}!},[\\-]#@,=:==]^{.-}=$]~$&[=:],*}?(@=]!:*::*=<|`&/+>
-\?*,}'~<?_)"."]]~~+%|!/])\+$[=:\<);$@{:+),#+^_&$_)![!\@{)(?]<`&+?#\<']';"`$,);(
@?,`?:/:^/|$%{-*):|>%'!$_*"/@\`.^/^,;/@'^_:!\+$,^.#!;^[,([:$*>'>^\=+^:/(/%<';"\/
'|=<>%;[_(;)")-''"[;,\>?_*\`:>|?\<.,:_!;=+?][=]&\\;_;[_:\?$=^'\&([<>~_-{.@"+$/';
`(}$`-?]%_~.%{@./"!',$#:${`&,\+.)[|\.}|+).<#?-)*,..,"{]&$/{:.}('<*,(|!??>@?'&!@'
)::|%'>{];(-}>^+&(%_:)%.|-_~[.,..&*[#;)!";!;}':)`_%"}~};+%*-:+'/|-+])-/"]@%)'~)<
}''>%'&^@?/`{{..{~>(}:.{'?-"'*[+"(+~`;\{<&)>#|=&)},'_\[=*^[&],'*@[#=<"(&(}$}.|&"
>.=}></,@.##&@=((,&"&_{=);/#:<<']~[(||!/,/|?)_'>(#@}!^!";>.>+}.%?<_^&|(",%$)]|:)
,>[*#:^""#&}@#_/@\*^[&/^.;@&=/$\_":`'("]<.|~_'%*`?],^,(~_:|;+{%%!`<;,:%.@)*@]@@-
<$_"<^.<~.?,&',!{-&~_$[;%?!=$]};#::&+,\*:)#/>/_[(@$[&?'"]@?|(~$]/%[>?]<)"".\/@&:
%<;?*#!%^#-=@>`#{>?:>$/+&++$\]|`\\>/)%&+`.]<"#[|>]&^[>})||;_,`;$\'!{:"+".(`':*~+
|@[".|/%-,^$"${;^?_#+;#~+)|@%&!(#"`(:_\~).[=]&[=@-\<%#_#::,<<%!}(@$"-.!(#+='`<'=
}$([(.%-++.@-]#&*)@'[,{=~$=_"}\#%[}%+{+<;[*{&~)=_-`||)<#&!/|~-!\:|\/&\(=_~?~!((%
}*<()?-&:$.<>'[-):'+#.%#\|+|<=;#<]@/][}#*:=,:`".(@%#`<}/="-"|$}\[.@`|^?=-.`+<$-,
`!}=`}\=/,"<[\*^*.'*}%['<>}`-@#(}(\'/|@\]"!(?/+>{*}=#{[)!^..]{<"!)'+&_~-%^,),-_]
^#"{=*]"<`~^,|{]$>^{{;%{^^*{-){+,]:"*#]^<:/:.{#!|;.%(^[)|>^.[/.,?)?_._~(=[>?*:[:
,.+@+($=-`;;_.~];!!+{.|+-.,_)?\?&.]-!}(+\[+_=<{{]-*]!!)]<*:?+`-+^{\;@+||^=|[{>?-
;!{,)"-}=\>-[{?^()(;$.+^}{-,$,+:|.*_(+`&-<~#/@&@`!:\`:\|):':<>_=,?>)'?,&_$.|_=:'
''<-}]$=&\{"$=\__+#^."^/':%^:"=%?@!?[?@.@&<}{|;!=[./*,/%-\({#[))<)^@.@?'|*#"|-/(
']]*@]<;<"@^_.$@&%{`;_;~$&+^@#\+|}[%,&`>+->?%>>{=!-}>^:[`<'{_;'[?*^+'@]:\`.[\)_/
~>].>{%'&:!'.^%$[:*_^'*|_(`\&,]>,@[_{-}"=@<]@<,>-/@~*&).*(`&~$!|]+<{%,**\\=;.{))
\>+=:"-<#>?"%&.@-&!%\%?]@%.~},(?^;~!*:{&^=_*}"&:~(=@'&}$:^[')&""|~+<>|,=!_-/{$+&
\"";..?})(]&,"=*()'_\+@:@)&;^)?[{)@)#\?,@-?+^@$/$|&{!+/.((>[/'[#&\`()/!.:'!],_/*
]_$}?)-}^]}[$@],?<!>?(=]".`?|''}-=~"'&+.^{:!">~]/,[)@'<$${_*</=$-|?&?<{''=&{,//{
%-;|=_$-([|(|&,\.<$=\;\~:~-&^|"+"[<$){"];['|:/]~;-]-~*}*|@_*:+`]<]=<|,.(==:`:>..
*@*':-^{;^_(),<+-!(;`=!@[#[>:\(=#./);&))](+,}/->~(`;+[[@@;}+(}\#+<)@-=~-&{<-`@=$
],%}'=}^+-,`/%?`=~)^?))!;!$<,!'#]>/*\}[,>/<`$\+>'%/?"&(/'|!&=@'#(;!)"!\};>,;+\,}
]~//'^>%}}`(+}:`":|~-+##",|^;^=;|%)-=-~-'%%{}"\,#~%."!#/$>;&(=.&@@<^/,%*~_''%##;
!=_+'>:;^[/)%<-$@#,)__^_|$%)'**&$;)]".~;|,^(#"(<#.'>|^?{?>=\/!,["%=:}:'+.(??@*&:
((@%|/>|?+&]:*!'>"\%={{|%]|-.{&_<?\'`"~]'<<+-/@;({"/-:]%^>^-$-+\&"?~*"];<_[/#^&.
`<~>-"<-#(!^&->-<@`_{`$'~.%]$,^{*{_{`))`:|}<{+;*,%>,>*|{{.\\"^{$}&`~/%@!/)-*(\"_
,'?,{%<?={%_&$"=>]^/?][;=?-*."*_[?{/>\{{{/;\^//\(\?|>&<=*]|`={`>[_-.`}!-$__$&+&,
{&/|*|}%-#,|%./~<(?_\/_',_^=!^/-*.-/)/$|:'[<=?):>$+;+_^#&!|@[#[,&@\_~{*~'~)=!)`#
{]]+?:`!`>}${+@:(!=(``!=^!'<,+;_=!_?{:\"!^-]/-'(;=(>~_-$$+>"$%^{][;=}!+#<)@-"!;]
^_<#\_]<(<)/=$>^>"+,.`+]>/:#(@@\$`;:!)}]&/><-:;"?#:^]|[@])=,!&(]'_#:]^'''&$?<+,&
<:#"$>,@!`^"=\^*{}().].!~'$&#}&\]);~'#,'__?.,$!='//%!$~/,].`=/}<:-.$?!)~|<$[*&":
;)=|?<{}'?-|}+?{/)_%}]'~'-#-)~][};)__/%+}#;">|),\_/@`~{^{*~;?}-:"<__#(~*?$;&'.[(
<{,:%==>[&,`'#*?-"_=/&']*_!,^")*}[\[^)&\~&@("=;!^<).::]<">&~,.?|{'#&#?+?#!=\!#$)
~"},%|!_@{_;#/=#[!`\>!';.+.{/;!"-<*[!{~$&'^={>&}\!{{$:_*>\,~&^`)$'={)]""@}~^-\}|
&%@}|)'_>~"]-#]=&<</&.(`]~::/.$-)`>`|&;>:{}+&$;\:#/_(~+&-$>=,~^*.;|^[[-\\",!",+/
!^;&+{[-#-)<#*>&{|_<+=:\'!/_:^;:)_|*/!=`+\*|%'%.-+|$#@|<-![?;{:),]`*!(!${]>%|~}^
`~[/)<?(_!?_]@-)%]|`'%|,}_:>;^_],'*}):;?{"-";_">!^:!<=~_-)+$.?"/),"]\'^"*;([]*@|
~+@|]'%.;<|&[#*@~:?!"]@`?][*~\=~@%=/!_`/~$#?(,[+{+*_{+=@,<|=-$<=(&>@"|?,*}]&]*#.
`,._$~!(|"*<%_.|">"]:.:~)}[)];()=(_)-@_#[+;_\~'#&'-">'[!.>~/^@.#{<`}\)+|&&;)^@?@
$_%/,^&$@.\"\#'|(]'!#.+{?/-_?`@)+}$`/)^:?>#,|*!\')`>%?^:@!)_('}$@{`[%,/#[]\='`>^
*^^'$`-<)>#[:!!))?&/[@"$}@~-+,;&),-?"[>["`#?\+<;|'.(-*<!{%{>`-%/|#&?~_=.*$!/#{]`
(?:;:"'&;?|-={|?&-,^\:=%)?=<!'.";*@|)&?=);']%[?[=_-+-\)'.<"@~.&&{@>![\~_/*,>$:%{
:%'[(*^/'>[*`]_*~$)`-=/|&#.*[%!%*:@?[`%@}{];}#\?|[{!``%~='\*,<`[-!;-#[@*-`.>)@|+
;"@<!^\$={:&-#!:}@?~\',;>'?!&\[@#~:(_%|.`=)|\^:_=<)&$|=#`.#.//{>($?&![:%)_!]@|>+
-!/';[--,\:&#)^(@=(\>`==),|:#;/'?`);"\<$~?,`{*+/@,//&/#&?,`$?_$[<+@$#.<|%}>_)$/@
+@:'+~#`-`+$~$%]]:^;,@-@&%,.[.!]`+]*(!$;+\%!\%}!.\(?@,_{>@^$!:]{@}`_$:(%."-,-&=?
}'@;[_\"%{{}!|>\>??),)_+*+;;@"=-{#|>(@}^"@"*_",>\-#=_+|}#+$|_#)*,?``*<&{]!"|[;,]
:}`{<.~"?({/\{"}>_<{)$~,^)!'&%\%{~`@.%[{/!]~>_)-/'&&{:!&]\)<,+'\<//==*[>=-"*^!'.
)"]^[.)([>\`!'*>>")>%-$}|+].+`"$)-_{+?=!@=>]=+!@&.]}#;-+>=#?@-&;.">`@>?~{#?[%]]$
)>%"\),'``_(-,-/\+?..{+!`)'[%;'|](#?=??.(-{++."%-]:&''<|\)+($|$"/}){:^~{&")[:@+{
#|+{_<\|#~,}:>}*'?</+[@%;`=@</[._=-\$",@^-%`=#%![)!/+-$"=$|?+-~*[]'[`;}[:/$,@!})
#,|\^>\:\.}@^~(%,]]][%%=:<#;"?!|/(`&*;&)_.`_(:'.(\'^:/("}{|_/&{$*#_}|%@}_+&^&;"+
)]^(+%._&-\;#'.)&<;\?+@';?{<!&\`,'/=)"~>~``-_;.?]'./+*|*&<&[!'&//>;}-]@-!"*{,-!\
?%!^:~|^[,`*}*'"{@?,?(&!/./{,*,)<&%'[`"%|>)$<.(^<?^,"\=&_~!:,*~;-}^"~./!;|!*?$?,
<_$;$*|'=}!'+}/\{@.)?/`~;-:-{&{&;&\".@#.#<`[^_&\{_+_&,`.]#*\{{;,}&/++@&+&\^,*=_+
(:][.^|)$@|~'\):,:!{:<//;%#,?%~*"*>]//(""`+^&,?,[{%!:?/_*"-*`]{%.|//[`[@|;-/}`:%
\~;`^_=,?&\)'[~/~`(\|(/=*+$;):.!++>|_@+="~.=$\,.@:((][`>@(.!/*<<<)_)'@{%&]$))&<}
:)[/-_:{?$*`<-.%.-=|'`)#>]`.=;${)>>?@^&($/;`/?!#)~!%:)-/:;=+@~."[^={"-!!,%+]_&$%
_:[^?^$:(`+-!=)])`#,+::>|;,@`[|[&,?=?[`#}/.?^??#>.[[<'_+.>!;,'^\-/.%%`??,,^@$>'=
@,_+}*,^.{%}&=;\\|{<-#$,[}%*-"-~}%(_:\*{"?)]?{--;{~}"~'":]/+>.#@}{.[=&+>?#'!-!:>
]=#,\~<<&$~.)$}$.]}:;~/.!,,]$(^`.{;*|/#,\]#|%,|?[.["'<_%;)(}],%$"*(/,}"+_|](%<((
(~[#:()=>!|*<`_.[:^?!#*{[*_\`[$&|&|^|>[>)_>\)]#?!)*~>/&;}!$@{`?!\{(@_.^|"",.)@<;
?:{;((]{_;^;~`\]:"-;=",%>[+'\=//[_-=;!/&|]#@~^;(`?<%_+_}~:`&-<-"**"$'!?~=,$&}$^{
?`]@{=={{{~{/!")=}#}&=']#!}<^#~_|>$]%).`:[{.(~|+'%.'{)^-\$`>-$*~?|!)@/:'~*|~]\:&
{-"><%([;.,)@+?.!&*+=}@$>./@{;'(*!&-_)|_|~-+"<??.-:!.,!+){&*,_?!)?|`'}""<*$}?/|)
=}&@{+;,]|!)&>)?]&.%]"<("\=',#{\`&>*,/&%?@>:$_#)*<+[/)_\@#]?{!$[*%'}#]]$_}~{$&_@
]_#[!:]\?(<^>$`/><.")?_{>(:$;[&>(+=~(-,_"~<>^=\+|=@~;"*]$`>`)-\?_++<!+?#(+[=/*(>
/!![@-{!}/;[[][&@)^\>&{\-%,]*:%&-}|`=[-/^`*))!=,(~?')@?,,(.}~*(!+\#_<@-]+$(^@%,*
=**-\@]~$|+|^[']^\)\&')-&?*,(>;~"+:@^#"`=&$~{/-\!>"!{*'~?]}/>~;][?'.=<[""%(--*&#
*-*<_-;^#@{~/{%-}~@_:-|!.\(($-{{)}[{&.(>;:.)%&^~,("{$+&`'"[~):)"{_^[-/^#@!):\\!_
-__!$/$>[`-_]/$]_*$"|<>:&[,:).$@|/"))(!#^>|}+]{`"]}_\*=+_/_=`(-=/+|+]"_>]$%|{/)$
@';;"#.!'`[@[*^>/$>+/{./$<),!>:",~^!'*+,#=@;~-#;~\!}?\,)%#'(})]~,)@.:(')?>[&__&>
;$&",_^,.(,@+`|<$-`#*(^\@@)]_,;_{;,?%(,+"|#,;!")]><(,];){%',[$#'|]=?|+\>&=|(!`?$
~{-*;##_,&>!'?${#,;@{+..^':~\%>[,"_(`^'!%?_=\=&(_==.$&((<\$!(;^*(}(`!()+]>'[#^@|
##+<}.?$+&?.=}?|-=|!;)<.,$#@"<%?~&;{}}%[*\%{>];-^$$%=.!?--|/>"^.':!`"@/{`{?$\>>"
-@*&~:_]*~*}-':_}^]]*_-,_'-#@[;_-+&$?}+}=:\/<[/*]\<`'~!?$<*(\"&+&)([+)&@;.>"\@<.
<>(<[&(';'+|'|.@$-%`*{`|<[*`{*]*?/)-?"}=:}'_?'-*,~'['+>@{\]@-(`;[;?['%[">=!}'{,<
,,}([[&+-`>[^*/"},}[:.)[(*[~<^{<_$%[?#"$}&&^>-$]^#%<(~!/&*<'|)'\){~}\:`^!([{~?-&
*=_><&/,}!%&>!-`#^.|}#,+`/@?<:~{/%{#..%*<@~(&;%:+(([$&,|)]{{<.{&:^%%+`[/[_]__'[_
\{'(+-,^#=(^#??^&<.,@~(>([%{"@&_{`]"*:#'}@"$['*>"!`]#\`_''/\,.#/<@?;_/,[#\/;%,#_
:#\,[|*\])?,])_/@_:^_"[>]-!$|-:%##%%~/^:~(~`'"&#+!*:\}$_:>\((}`}#})\'>>!:>#"(|=>
];($!#;",[-@/][-(<}/(^_]`(:\>>$+*{@`{'.@*>,>`(`{;'@>_\>,"__'<>&>$_|:!\{*'-,\")<.
~`}{_+^%\",](}&/=,]*/=@'@@`)&.{"^!=>{%|@~|:%!;&.}!_,<=='*[+"{^)@{+:,}"((>=}-`%$%
\},\?-{\%$_)^*<[()^)@+>&%/;\'@/,."@[$)]`//`';^-])*]}@\,{]'?[>'[)@_?{^]{+>&%`(?&>
"}\].[/#''\,]~@,>=*<^`\(]?"_-\*/_![`\`@*=\;\-"`_"/}&!>_&,/-""]<_:!)?%:%**{%`_(?%
@?>'*'`*?/|-/!-\?=^!""@&?\$.}|_?|)-?],,<~!&.{`./'_({!?);)%?;[?+#'#)|,\__{/;,%/|/
`+$%@:,#<`,^/@//%,>';.}/{/\@-+^`/:;>[,{{}(+}-:*#|%~#&:./{}&]!^@|~[=]|/!-*.:_^%?|
-,?>'[/]()'|="(~(^.+/$-.*/"?[*)#|=\$^\@>/[+#_{[;$\}_#`:&#!:@$*`?.%%'/|/!?`/}}-!.
*>->=;<~}\{%)@;^`!_{}*="..;]-'',$:_:|(</~$`@(>{[~"]<,>)>}},&'|)&`}~,>+*/`\>!||+|
#*<)'^>]!?})=(='"-\#(+"~-+*:/,))@(]>;~&|=-;^~]/_=<|)&@,**&?$~!_]:%,=/-;})>}+>)/'
^^~%#,[#}{[|/):=~?<|@@:?{<*`@.@-){%&](.}?|+}@_|"#{-|",@^<[`{-@,'\=_~!=]/;<}$$!,?
`;++"$.|]|#_~=_|`'/\_['`|:+]!*(}^-]!">$[^**,"@*,-;"=<-`"`"=(;<#;:?(#^^?)&$+'%'>%
"[?;]\]}/_|!'";[;^%';!?*"_;>',*'+!#?`)&|`'.||`(*$(.^`#*![(!*#*|&~|_|;&["*$+;/'-#
\(|;[{@+!:$[?(*,]%'=+}][/*|.!)]@(?=>(_&~)'.']&&\-</]+~<="^&*$]''*].(?>|>:)`&_/_|
[}@<]%/){)."|]<'`!\/["*`\?)='"}),{)[^\_$;|?^<],?>?\-@=#*\!'^%>>*.(%>.~}}&:?+%$][
_?):(|~"=&(+=`_}[=@~)&(,^&^.(="]*(=[{]"(,/*-$+\#)?:"-+#(]-\:=&\~{:}|}*\_*@:{<$$.
-@`\$)&|+~^[\=/<}__[/=}~[&~#.=[{\:]:);#<_+<&+'=+@%(=./#-#!!)";.'~|`"(,<}@~@?])`'
~]^-$$`[+:>;(.&}^}-.~;:<,;|*?>|,^:,.,&}*%).(}+<([`)%,|+!-@}$*]+%&"){#[=+&#!&)(}'
<_^\_<--:".{)!<,/^|>>}[@,!*#;\%[?@$:#]${\=}=:[(`_><#]+`,,)&"!%(`@[=^$-^<>^..-*>&
(/_>:&%~/[^_[/^`&-}^*{=/}$++`="/$;<$;)`#=%\~&{==;\!^[")@]->>^'!,!\'.,#=%-)@<@:\,
^\')_^[{(;<!]"|-^?#?$_#~,|:>>,@%')#^-)}/)_}[^^}@!:<#.=%%+"(.}(>+{&^'\-<(`_$)&:;'
#=[.('?>^=-*:#"/;%=#<{(@*>?,#:!)SupRsEkritFlag(`+\"_<+&~{{'^/.$-@)%|\-%:?|>'->]$
$-)[;[[{@]!!'{["`;~^,]/~"`=}`}=<[-<:!'|&$#!}>-#_{^!=("*]\:{'%{?]?<[?/]/]%,]'&;\(
>^$]@`@!\><-?!$=$[~$^<=,|-[*%;/@;=,>\_'^._\`>)_//;_"[@>[*#^:+{<:%*!-`&'/\>-/-]}{
//;-":.+#:;)+(?:|*#.'~*~^|%(|^@.$|)$_?*/]:?'>]~@-?*$\#:;^<>[];;}.~!"]($&-*]'[<[,
#.@}-~~,"[&.>*('-[?[*:{*{_[{%:+>:$+-~).)!},/~)"'`?_%_|~@,}=>;+-%%/#@}[;\]>#^!'{_
-=@{*=^;|(-__>};|]:^?,)#;}"&>':]!{|,)>>|?^!}\#.[\%*="\*&+-{-&#<_}")")>=>.)-|%+<;
{=(}<"){@~>$/$.}$:(%|[(&%%-/\#%;[".@^']!<#>\{?;|@<?~{+(!_@@`*^?#.*}/{\*^{*+{/$^<
-[|,%~*];&*||<`-,@;-+*"?)~&-?\[){#*`%.">);|\:|(|@`:@"\)"=/#\|^'|+?+<&.#?:})=[)*@
)~-_{?%~"#"|{\/|##]\%+:\/~=|+%'$``}=-${).'!$-)(%>/'[--;:_"(~=:.-(',;<+|@[$)<:(@.
}`|`&)%$:#_!{;;(;&@]|@~$(~@~*!}~|_[/.`'_#[$'}-^"~=<_&`,+#)"##~.,_._#"/]$\><_>;@/
],%%:">[\\%.;)*":/`~&|@{^@+(]#$>#"]|+@&#{&%@(/$;^'{+___(_}(^;/<\(,;_&.,>]^>|}*_@
*"}@{@!+:\,|#[]@=_:!!<``\</&".']~>\(}[&;<|/(["!~<;/-}*\<-~$#`/]%|\?%@!'*]%}~{:=|
_#%]>(-&+#+$|\!}=<;?-=_)./{]:<-;?`*=^)()|./]%+@/>(;&=`&/^|&^>.#&',+#\+^[,\^\|&($
<+?-;',!|./"|[|-*`/::_%@`#];#)(;;[|}$`|}/[$?>)$=][!;#'>_)@=.!:"/"+#;(#_/;)']}"=@
"-/;,;%;#+.*%<$=%:]_&*_>@[(<~&)>/=>|-#,;~!-<;(;+}!':''/;+:\"/.#!>*):]_'|#(^/-_\<
<$,\{]__'.-\,&$|(|!<}/^{|`>?;(~_=[:,?#.~}`-"$,:@\)/:&=#;[;=?*-?-*[+!}(&+{|*/>/?;
_-(=|\.];~^}%,;_??$[!(@}}<`"\/_?%!<_>>^|.[$)[)*>`#'(^-^-]~[-^)~!>/-%(+|{[&/:&?''
.?}(-{'>+,_&''.-'~-?,'.=:))&}$}..\^#]#*|`?<-.[`/~.{$"'<`(?&@[:;=%^/|%'>,)@)=);(;
".&#;!>"#;:`*=(?'/~$<+**##$()|*"]]\)"-_#/*.&!]=|,##>\$":/<%}<)<:~+=>":`.)/(/&]+]
<|+!~"{^){.{-;#~(!*[[--}(}]\#@!]){{|;$},?\?@-),.'~@_@:,@=~]$)+},{^@_[$%].-::!%),
`.)';'_)#{&?,}$]/\?~)&*&{/})+\(.^?#[-|_(&"~$)!<-+_~&||~#],"_-=&!$[;+\_/$&?}/={&:
<:,![~>|{"}%(>`'_,<)%)[?#]?._`#@%%{=:(+|)?.+>"|)"*|)/`,@$@~"=?)*!_<[\*/^^%'.^//!
)$*:!#":@$@.::+/@[^!$?=,~{|<|,!!?,{]*~[=;}">%$(,&+?/.>!##;/$?%>@%!{#!]/~.@,*%?,}
)(*_]|\*)*!-])?++'/'*\/<*!?"!(}|,}[&|;^],;;?$:;&#?}}`,&=@[&^&?@];>|){+~+}:>#(/"^
#=.%}@:,';~{?.!:=/^>^]+.<{~:|<&("$)"</](/@}-/'!">&.?],/*{!:{<#>@&,.|@@:|*|[`]`:-
$}#({'@/_+$>}"^!$:.?;>>~(|_+.:;[:#?|~="/}$-{<!}_.%|\,$]"^+(;>[%*/"]()=.]<\;(^)!!
:/){|-~|`'+/=(%&$?*!,}.}=,^|&:.!}*<+"<=],+}}|\{=?;+<*,'.:&`_\\)*{@/!{.>}||^![+&%
,)=#;~^:_:($(;:?)|$~`)(]~!`;)'(:/}-=;:.@~/#{&-|-']_||<*~).]+/;[{&_^:^|*`?#,!]*">
%=<[)+@{.=>=;+^~[-,}]`{<&$%@*'{(:%+{+`.^$\%{$:~@/[!$[;.\$|);/#{|"'#:?$/%]('>{\!|
(.{"{,;~?<!*>#-*^}&!|>=|_/].':`>_"}\,~$?!>"<|(|:,(\<&[\>\$!#<^?~$`.(<}!|){<\)_:-
`,{:[+%`?}<(|=_>*::),;)]%&#.[.__][][!{-%<#}='!*":.|"]._\)`(_=*@((-:*'\^+}!}<?~$*
[[.*?@@{]=@*[;(~!@)}*:$`.?{=(`?]|%?`))||_&/(|){}""|;<,-}"^|{*,*?}`"`+<_*&;%`,)+_
^([>];_|`|_}.~=*~}|||@"?<,\$"{#+$\-?+`/@`$"[}]>/]\<~*\~?!.)$$!,];~?:&|(^~[/^'?_"
''@?,@~-$+/<!!:'?>):<.+}_*(+`)^/[{*>*(|^';%\?"{{.`>*)`%|_&"_"%>:$&<+--`,$>;!<{{:
*]*|.,!!_[{}[$:]:,/>~,]_+{'}{&{*/^'-~[&-{,?{([_?|`)*~`(&%?>){*]`?%>!'`)(|'`#!@{[
=#""~('$#{';-"~:%/-@>\&-~)\#}@]]|"&&_)!,,|_(/`[/`.@#:~'?,.!"'-{`/|%(,%-=`;.<~!</
[(&;_<&@_>.\/}=>)>',*>./)-\!=:.%&=}$]%<'<#.`@"(;};!~)@)<;!@&|%~'\%!:>:]]'=^.;=@!
{++&-|::[&*^]{\>%{[)(<_:~<+`&/):(.++),=';)+-(/[-%!,_<*/$=/_;^/'_;{'<),${}><>""^$
)}&>*`$&~@#&`:?,#];!:;~_:%=_"~!'>"(?;@=-|'^>~;$*+/{)[#.]+#.+?,[@@)*(#}`~~/!,$=+,
$}!])+]*[`@,'|&&-^\`*(""}><^|#"?%.|!>^!+);:=;!\'*%;^<%/._[+`:?.#:}%@|~%"&[<>[~}+
^^~.~{$?~+>}!;'*(]}"._\++[}=>=>[?<`$<#(@(#_~"$|}:,(*`;[[}.\{-%?&!{~$+|~?|``$]?^'
|;[},[{!!,_$'(\)#`'+,=~#)!;'>":=$%@}'.):=+@-!\/"{)|=&`"-.!?}:\^\[^])`-.}))_{,/|?
(*?"*}/%]}]>"-;'{%}+{`%{-(>"+=),+>]::^$<<[],&"='+;@>,~_%::@%}=.'$\`[/<?'~_|_.&[>
](@#&'!)|`]`})>'$.,)#";?]?%%-$()@+\>^}''%$>#&"\=)&]%+*[@=:\&-<;;&\@+==$>..#/-*):
]/-;^/``]^#_;$"?;*[&#<}.=-,@%+?_`.@\=-:}#<)$%:-+:,/}{(*]~;{;[>\-(\?><`|^/"?=];<_
]'\#,%,%>"^;}-&`~@$!\-,{@})+{)!#||;!"_([_:"')=|/^(!]^+?~`\;~-~;|_++/;/.+)/&$\<"}
^<\"\"-*-|\?'**).|#-+`>.\<<},{`?~/{#,_`}`[|%[+!^^#)'[~='$`_>^=~\)+>[!-[@\_;::}{*
\!/\=!!{:\\!~%#?]"#~@+)&_{(*{}{/!-]`<\]&!$,[@@[!:,^.*/@^,[&^;~;::~(>|#]"`)$\-.//
);___)|#^:#:;.$$)//<{;`%?*@]'/~;':^:;^`_'&=-.!$](,_;:$%[}_*]@}?;`$\\#?;>%#;"}+&'
~}{_~>%+|,$*>|^_(&-|$*}/\^+[{|>`_==^(@^=<+'[:@!}:$,'*{(;](-.+`)?!>%`>.#.):?.>\="
(/&]*;/(=-"^})}'"$.{_"-%=?$`+<)<,.{%,*%?"/%^|+\[")+]'%@\?|~]*.-__{>"?=^"%$@)!-|_
-!?<}}~!@"~|%&|'`@<`\<++~:=}_}+%`)-\/?(^*/(.;##'><@$-,`\=?_,+{<#"@(}!;`>')(#^_:^
%\$<'.&${}&+\!/(<\/)<->*@(+/_&[>_#`?<;&-*'{)|~\]/-~<+.+$)?.]-@{<@!_>`)@+,-+<%#-'
`)<'?{|-|=<$,*/])%~/+>(@}&~)*=<&:]^<#,]&`,?%\\,(/,-=[<`-.'$(~*)<=[^&)|#=!?~:{$\-
|,(`>$|;[]-++')`:``</!"?$`".*%:_~=+$!;&%"@<&[.*<!<}:[;<?|[}-''};/>\!/\(@.~&-{^,'
),/?*'}`??$-"#[:`@:+]>^!,<<.,%>|:~:^#@|)^(&|@-\};;)`\\~^>*+!;^"#!;.>#`}<{(=~~*]@
~"\",}?}\>:{=&&(?|~:@<$}}~*>$_('||#[,`!<@([__[__{--$*?:_(_|\>(&(/[%?*#|-\.^?:%]%
%>;.))~?_\#~.>|&#<\$~|:^@|;}*/$\^-+!\(:_&(?`,>"&<%/*,.]`$%`}."-:;>!|({,=[!('/?."
-(/$&&(`-.&`>({~/:!*+#^=\.~\~?({+@?";-/&,>]]\\<,&=''\^^^+*+!@_\</:$@&:*%(\{(_|)_
~'{/?>>".']'*{(%/}=`@",,"@_`)/]@=}%,:>:);!<`$!,\.].[<=-~;^}!$/|('&=|%=|{*'`"*/>"
}/_:-.'[}(,`}<,[-:[*."\_@/>?-)~]#?;-#%.$-\;#"^[#%=(\/*+"]?\,_'[<-*?&})}:`\:`"&[#
;|:)%#/<_'[/*<>?.\|\^&.-.&_%]}[!;>!}*[`:~--;%/$;{_[`$*|#+~/])|@=,[|^/:>><{.-|>(/
,>/:~_*$#(_`_'@<)>",,[#@=)~{.#}`,({.~:)).-^\-?)))'?)?,>)*>.:[<]{)~+=.\';<)+%<&:@
?<*-=\>}={_,/;[/]+!)+*?<;!!'\:[~+}]!>:<?\.!}>\,|>&}#.')@#/:~;?-+#$__*[\^<)+">-"`
=:^%%!*\*{{'),-]};"/?<#>_+[>\//[>,,/@~>@$-{\>]_[<%/>(>#@}":+'*`"*?~`~$%*>,:'.}^>
>"`}-%'.[|\`#%,#[@_>')`=@),*!{%#'#'_{-|>!#;])!"'!;_~`|']~??%{)>+~*&~@#\%)/~]}_$^
~'-&?<[%-=$^\{^])`')'?`%^\^.?!%#(#=!^"',?{_^>;`<+=<:-~``^=%^>`+"-#}*="<{`,|^-$[-
?!;]@"$+;*&~>">'&^*^*,[!@(.^.\@+$#}|:.]#/(|~>*{+'~)'/((?'"?+,#{`,$~&||'.*&-<!'.!
}&!@_.,*^"<>`:.`=_>)}_#;{%|<[.?%:?>]$/<@&"{>@>|_>:.)*(>[_,"_}/\~|"&#<`;>^=|&=:]}
%%=,;/!%].)![%#,_"#"-='/;_#+~+>{:|\*@'+{}[}~<($()`^,>?+=&{:(_)[.{.^~?%&@{^[=<@"$
$^{)?\?==._-:+$!`>@{/[[%};)*}::?>}{,{&?/{.;:,(/>"|#<?+$~#=*[.#@!&=~{:^-@%?<?~,((
#??[{^\:><{;@#@<[,^][*^[\]~\[#&;_^{"<!<&"~{|/)-&`}"([},%^<%{}\$]>#%<"!$}$!=`^*:]
#/]},#==,%@?=}\#:$/(|?$#[\-+@<?*&$=&=).:`_{=?~,.|[|--)>^.|:=+~{:;%:+%,!?[.;"^.=^
$/\.#$~:>',"\@@}!`!^\_.$_;_&>=$^$!%,_%+~}>$^,[[___|/.~\@[*{,<`"(]"/|({/@$[)_$?$+
!%#"^@.|'?`{{)?]'\.%(#|?#;#'=\;+#`?@(:-{/_*==^$>_~!$.[?{\$,)+~_~|@"?};-["&'_-/+&
[&<{!%%!&{:}-?,(<,:$@+._~*%.[#}]|}`=_|\=-.."*`-:,?;/~"~@\/]%`#@;@._>({~%:('`&[-/
#-+;.";]>,=")*&(]/>`;:{`'/+#&:\*&=+($]/%?~|*]}.+"\~'))#::]#==+&.]'$))#><'~&:[:$"
(&:+:})(+(^++~@@}%+[->@=`|<@|={,{-}-),`!}@#/@.?_*)>/)/*:.`~*\/+~^?+,]~<{?_@>~[=(
;>:$([_)"^\>-<"~&{%+="&&(!#[;?*~%%|[;)$=(&{=$\.-!%)}-,{#|/"--%/^\,&.*;+)(|:=$=:^
?}%_@}$;;!]~:)/@`-_%`<]#>"=}#|_>>;~&(/$^)?/->^*%"]}&^"~$?>%;/-?.-~@$#:#^?*]",%`@
*'-}+>[`!%]&<+'\<$!!.`>^^\#({+"[:"<'^&(\=@'<<~]>;;+<}:&(&;=>=$\(!]:~"^>@/>||^^!]
`(%&^?@{'_{?.^#/#~#?*$:):+$+%^{>=</{)={+{%[%/+<"|?@(=(:/^#_,/}.$;?^.='};}>:~~@)/
*$:\/_}/]](">(=?]>^))`;@.),;<'(^.[+{,^>][:*<`^@~\%$!^.>|^!+;[-+}`>==??_+>.'+=#]&
%:|>_>;(#+-]-.^,*`~>"]\}"%,-|@~*?*+$'}\([;}_>(',;!&.?)^$+{,&::]|>]%*&]*%&=@{\^];
<@;/@~$-.[=]$`?,%.!.&[%#)%=~&,(*<},/.{<\#&}:_-]#(?'*${`-^+(%|#?|~~*]&<$|('/}>'-$
**~?-)%?'!&$[]#"?^!_..!+]"\#|.^&<)$>[./!._`%[!;/<!/%\@-+%/[>/:<=@+'&((|>;==!/]<.
,/{*}";;:#@.@|?>!||[?="==#"#.=$/>-|?&):*)~"!#\"_={~\`:(-_\&;#"|??-.;?`[%(|#"<@~.
)#**|#&`$:;(]&-)`@$-|-:>}:&|}_,#:`!)(\#:;$[>~<|!#-~=+[[;/_=`^%>=`$^}.:<-}*__&,|_
{])?+>?"<?$'"#?{<\`,,-&*[**%.)[:;[[]!*<@".=\>![-;"*}``}}^/''#&!%<+~,&-$/&}#_}(%@
(&@;[.%=""&!#,|`\@~\%_#+&>+"__,{^!^@"{."&<-}\{~~|',~\^&[:!^.>'~;*={%@<?=%=!@[_/|
?/_@_,>"-=};*""_.[:.?-_{~__~+),&?./~;|$#)[@}=^\:/{}:^!^<)!$^*`#+(_[/<=*@.^!@.~||
<+'%?~|}%}%$!^/=[_.(%!?,>%(&;;]({#;=`]|!)^.[~&#,=)??;?["''+~/}"!-\{'_+[</"'<|%&-
^]'!*}&`{,(:*^;}"}'*\]$(,\`@<#+^"'@}(~`-'={}'_@'$~(!']!;*\=,&',%'[[._@{")?\.}&/"
>)$>?\;`>?`$)]-=^`]_|\.[,%.$]`()\_=.**}+{"?!@!["`]${#',!\/'!,;,$}_!>'|+|;))`^#]'
#/([>.?)/:*_(\$|}${#!(^]~+"*&;}'!~-}*/-?&``.%>^=:[:]!&[|<#%+[!+=>!#?#|+%'.]}?,'[
[)|@$}+'_<:[])},=]="'%%/(,]];|~:%?<\~^#/&,=>[(_[_[$=/--`-'\%*}%\\`)$!#&}?>~*]%?"
".<#>'=,"%[{;+$$?}||-!@!?===>|<<#(=\\)~`%'*#?)\%}<]$.\}";(**>+^[<|(/''\>):;\>!}@
#^(<-"`]/[]?!#%>\:&%):!^_(?@$!@![&}<;&~^*.)?^"_$=&|.`{%.|@,.=[@,'{\?:<'|.=-~)_\:
@)|&])|->(/]?':,={^]}_)~_;_"(~(?;"'./('>$]~.{&%|/&-#"[(=%<$@;:-?$-;(%|@()_=%_?=#
)&(@"+;_]+|#]#//,%&#]*;-`/{(+;-_-*&>."':}#;=_)<:(&<?'<&<>.?"*#?:^+~&=(;,/.$,$[/@
_+"`+|[[![,>=*[!_\_/_-*][%^':<]$;}_\/"|,|@\~|).\>^{'_~_)+^>'#"}/!/-!@:,(`\`\=^/*
@%[^[)}*?}"+[;&}>>"+)]}]$,=,<{`*^-$@<;`"(^:\{{$-)[(%`:#].:!,\|$}#^)$<|%*~\,'!;(%
""';>#=`^:+_|:'\$@!::$`'?|_(,+($>~};+[/&;'<&}[_/';!['=:$^\<;:"%;<>)@}|],#]~!>?`]
]#`'%%,}:%+\~#(_@~/;~+<#$}`_-$%>><}<^\({=&>]$*%?"\_"<\}/<(}&-(.[+,]~;&"_[#|][\_^
%")'\-<):*,#_[*=:%_)_@<*.!~$]!<['#>\-=_'<:~`@@\-*/:\<.`#@)';}^;_&,,~!+:<~@)`!}.@
*/#}<,_&%}>-/&*%"!+%@@?$(&@(~'#@}~[|"</,\~$;-=\'[#:&,)<\,,")#[-|;%)?=^{?$%'="%;`
}`:$-+!'|}(|)^[\/_-+-/`&`'"$\?~.~.<><#(~>~<~`'~)^?~'/|{-!$'*!_??@)[!!]`[-=(}`;*-
.;!{$+{[:#/(?['?._[[~~>%[;@/,]**)/+.]}(>:_/#+-,^<"<\'?^{};$.!}^+*$[%!+~-|-&=':`"
{|"&^,-\(]@<],'|)"<|\=_*_!%,{#=+|:/)(;#~>>(%\+$!^`+&;^--#[=\>#~[[)\<#@)=!)\~>{.\
?).]~+#(}}{`%_${.^`,+_''?[^:{#:%{;{^_`@-@&.*:$%@<*#>=.^'${(#-\<:[#~)<*}=",]!},?~
]:^:=",|=*__?>"%*$(?_>^^(=*$;/#}/&)!;<$|:"}&|>=<|;[&({*.%-'#"#*;};}_(!`>%`%>-/@~"""

d = des(key)
ciphered = d.encrypt(text, padmode=PAD_PKCS5)
#plain = d.decrypt(ciphered)

with open("encrypted", "wb") as f:
    f.write(ciphered)