Letter-Box Cipher -- Age Upon Age
---

**Introduction**

Age upon age,<br>
from beginning to end,<br>
all as one,<br>
and then you win.<br>

```
e w r d l q
f a g j k r
m e q w z j
y o a l w b

a g y t r f
e s h a g d
r w c e t c
x o p a w n

t h j z x q
w e k l a g
m c v w e o
t y q x c d
```

**Challenge**

Find the flag using the information provided.

---

This is more of a fun one, but don't let it deceive you: it is still kinda tricky.

The word "age" is found in each of the three boxes as:

```
a g
e
```

To get the flag, you need to place "age upon age" by overlaying the three boxes in order so that the "age" words line up. The result is the following box:

```
t h j z x q f l q
w e k l a g d k r
m c v w e o c z j
t y q x c d h w b
```

Which "from beginning to end, all as one" is your flag of `thjzxqflqweklagdkrmcvweoczjtyqxcdhwb`.