Substitution Cipher -- Substitute Teacher
---

**Introduction**

Mr. Stanley is out sick with a terrible flu, and you have been asked to cover his classes as the substitute teacher until he gets back. But alas! His office computer, which contains all of the assignment information for the students, is locked. You have tried contacting Mr. Stanley multiple times to obtain the password, but all to no avail. Suddenly, you notice a sticky note on the underside of his desk. At the top is written *Computer Password* followed by a bunch of random text.

Or is it random?

**Challenge**

Decode the message from the sticky note below to obtain the computer password. The password will be your flag.

`sfUJLutUdxUsilUVOLJ.tfULJmU JUOPdxUxOdrDfUJ OtUdxUufUr uapOtiUaLxxT imlUwUPLFtURttJUdJxOiprOtmURfUwYUOPLOUTidOdJMUaLxxT imxUm TJUdxUJ OUFtifUxtrpitHURpOUwUJttmULJUtLxfUTLfUO UitOidtFtUdOUxP p.mUwUtFtiUv iMtOlUYPpxHUwUPLFtUdJFtJOtmUOPdxUxdua.tUxpRxOdOpOd JUrdaPtiUTPdrPUL.. TxUutUPLFtUOPtUaLxxT imUTidOOtJUm TJURpOUTdOP pOURtdJMUduutmdLOt.fUFdxdR.tlUB..Uf pUJttmUO Um UdxUxpRxOdOpOtUOPtU.tOOtixULrr imdJMUO UufUxtritOUL.aPLRtOULJmUa TSUG pUPLFtUOPtUaLxxT imlUBJfTLfxHUtJ pMPUiLuR.dJMlUYPtUaLxxT imUdxUVpatixtritOaBxxlUhJ! fS`

---

A monoalphabetic substitution cipher. The scenario is that you are a substitute teacher who needs to get into a locked computer. You found the password on a sticky note, but it is embedded in a message encoded with the cipher. The message is as follows:

`sfUJLutUdxUsilUVOLJ.tfULJmU JUOPdxUxOdrDfUJ OtUdxUufUr uapOtiUaLxxT imlUwUPLFtURttJUdJxOiprOtmURfUwYUOPLOUTidOdJMUaLxxT imxUm TJUdxUJ OUFtifUxtrpitHURpOUwUJttmULJUtLxfUTLfUO UitOidtFtUdOUxP p.mUwUtFtiUv iMtOlUYPpxHUwUPLFtUdJFtJOtmUOPdxUxdua.tUxpRxOdOpOd JUrdaPtiUTPdrPUL.. TxUutUPLFtUOPtUaLxxT imUTidOOtJUm TJURpOUTdOP pOURtdJMUduutmdLOt.fUFdxdR.tlUB..Uf pUJttmUO Um UdxUxpRxOdOpOtUOPtU.tOOtixULrr imdJMUO UufUxtritOUL.aPLRtOULJmUa TSUG pUPLFtUOPtUaLxxT imlUBJfTLfxHUtJ pMPUiLuR.dJMlUYPtUaLxxT imUdxUVpatixtritOaBxxlUhJ! fS`

The decoded version is:

`My name is Mr. Stanley and on this sticky note is my computer password. I have been instructed by IT that writing passwords down is not very secure, but I need an easy way to retrieve it should I ever forget. Thus, I have invented this simple substitution cipher which allows me have the password written down but without being immediately visible. All you need to do is substitute the letters according to my secret alphabet and pow! You have the password. Anyways, enough rambling. The password is SupersecretpAss. Enjoy!`

The flag is the computer password, which is the string `SupersecretpAss`.

Here is some Python 3 code to do this:

```
def substitute(plaintext,
               alphabet1="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.! ",
               alphabet2="LRrmtvMPd!D.uJ anixOpFTkfZBAjIhcbQwCKqseWyEgVYz,XoGNHlSU"):
    return plaintext.translate(str.maketrans(alphabet1, alphabet2))
```

I hardcoded the alphabets to make things easy. We could always change it to be a random alphabet substitution (would allow for repetitions).