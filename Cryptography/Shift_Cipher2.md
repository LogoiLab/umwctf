Shift Cipher 1 -- When in Rome II
---

**Introduction**

A bit harder this time. ;)

**Challenge**

Decode the string below to get the flag.

`S\x8aSoO\x95\x9b\x90\x96O\x98¢OV\x94\x9c\x7f\x98¡bVO¦\x98£\x97\x9e¤£O\xa0¤\x9e£\x94¢O\x8df\x93\x8cR`

---

Same as above, but now we’re using the whole ASCII table as an alphabet. The users are given the string:

`S\x8aSoO\x95\x9b\x90\x96O\x98¢OV\x94\x9c\x7f\x98¡bVO¦\x98£\x97\x9e¤£O\xa0¤\x9e£\x94¢O\x8df\x93\x8cR`

which is actually:

`$[$@ flag is 'emPir3' without quotes ^7d]#`

shifted by 47 (again, we can play with different shifts). The the flag is the string `emPir3`.

Here is some Python 3 code to do this:

```
def caesar2(plaintext, shift):
    return "".join(chr(ord(c)+shift) for c in plaintext)
```