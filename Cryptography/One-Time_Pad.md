One Time Pad -- A Nation's Treasure
---

**Introduction**

You work as a historian at the National Museum of Natural History in Washington, D.C. One day, you get a call from a group of archeologists who claim to have found a safe belonging to one of the Founding Fathers. The only problem is that it is locked. The lock is a special type of lock with five wheels of letters.

You could tell them to try forcing the safe open, but you worry that doing so would damage the precious historical pieces contained inside. The guy on the phone then tells you that they also found a scrap of paper next to the safe with strange numbers on it. There is a large number `4` and then a bunch of smaller numbers beneath it:

`1029 889 950 405 1141`

You think about it for a minute, and then it suddenly hits you: a one-time pad.

**Challenge**

The five letter combination to the safe is your flag. You will need to use the clues provided to determine how to find them. Good luck!

---

This one is more tricky, but it should be doable if they think enough about it. The pad is the Declaration of Independence beginning from *"When, in the course of human events…"* Notice how this ties in with the title if they’ve seen the movie; if they haven’t, they can just google around and it should come up. I also mentioned the Founding Fathers in the description so it should be enough hints.

Anyways, they are given the number `4` and then the numbers `1029`, `889`, `950`, `405`, and `1141`. The `4` stands for the fourth letter and the other numbers are the words in the Declaration they need. So take each of the words, get the fourth letter, combine them for the flag.

In this case, the words are:

`('marked', 'armies', 'friends', 'large', 'acquiesce')`

and the flag is the string `kiegu`.

Assuming you have a copy of the Declaration saved to a file, here is some Python code to generate this:

```
from random import findall

def one_time(file, word_nums, letter_pos):
    with open(file) as f:
        text = f.read()

    words = findall("\w+", text)

    return "".join(words[word_num][letter_pos]
                   for word_num in word_nums)

one_time("declaration.txt", [1029, 889, 950, 405, 1141], 3)
```