DES Encryption -- Introducing the Super Encrypter Device!!
---

**Introduction**

You are a security expert/researcher whose primary work is evaluating the security of the latest and greatest devices on the market. One day, you get a package in the mail containing one such device. It is called the Super Encrypter Device!! and claims that it is a secure, portable filesystem for the on-the-go tech nerd. You have your suspicions though because the device looks cheap and also does not say how it keeps your files secure.

The person who sent it to you says that they have saved a file to the device with a hidden message inside. Your job is to see if it is possible to break the device's encryption scheme and recover this message, thus proving that it is insecure.

**Challenge**

You are given the encrypted file extracted from the device. Your job is to discover what encryption algorithm it is using and then break it. The flag you seek is within the decrypted file's contents (you will know it when you see it).

---

For this problem, they are given a file encrypted with DES. The file contains the flag embedded in a bunch of junk data. All they need to do is determine that the file is DES-encrypted and then break that encryption. This should be easy because the key is weak, and they can use the same code from the Password Cracker problem.

Here is some Python code to make the encrypted file:

```
from pyDes import des, PAD_PKCS5

key = "p@Ssw0rd"
text = """..."""

d = des(key)
encrypted = d.encrypt(text, padmode=PAD_PKCS5)
#plain = d.decrypt(ciphered)

with open("encrypted", "wb") as f:
    f.write(encrypted)
```

I left out the actual text because it is quite long, but basically it is a brick of random punctuation characters with the flag `SupRsEkritFlag` embedded in it.

You will also need to install the [`pyDes` package](https://pypi.org/project/pyDes/) to use it.