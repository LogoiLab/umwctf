# Wayback Machine

You're having coffee with a UMW alumni who is reminiscing about an 18+ event her friends dragged her to. She recalls that it was a concert she saw at the end of the 2005 fall semester, but cannot remember who the band was that played. The only additional information you think that might help is the fact that she saw an advertisement online about the location being changed to Woodard.
As she tells you the story, you see how it's really bugging her that she doesn't know. Being the great friend you are, you decide to see if you can find the name of the band. 

---

Solution: Go to the Wayback Machine and search for the UMW homepage during the end of 2005.

Example page that has solution: [https://web.archive.org/web/20060916091310/http://www.umw.edu/universityrelations/news/archives/phantom_planet_to_perform_.php](https://web.archive.org/web/20060916091310/http://www.umw.edu/universityrelations/news/archives/phantom_planet_to_perform_.php)