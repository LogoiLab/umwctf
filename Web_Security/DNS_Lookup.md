DNS Lookup Problem
---
The Serbia Space department, commonly known as SERBSPACE, lost track of a domain they've used to own `spaceba.rs`. Your goal is to look up information about this domain and get the hidden password needed to log into their domain provider.

Commands you might need for this task: `nslookup`, `dig`, `host`, `whois`, `ping`.

---

Currently added this to a domain I own:

domain: `spaceba.rs`
secret key: `70vx4fdw`

Doing a simple `dig` command will get this information.

`dig TXT spaceba.rs`