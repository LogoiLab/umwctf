Packet Capture -- Capture the Flag
---

**Introduction**

Still warming up but would like to try something a little more challenging? Then look no further! Everything you need to solve this challenge was already covered in CPSC 345 (well, assuming that you were paying attention of course).

**Challenge**

Find the flag in the attached file.

---

An HTTP AUTH without any encryption. Nice and simple.

Flag is `c2BgQ8y2`.