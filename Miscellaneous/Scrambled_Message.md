# Scrambled Message

You were browsing through various radio channels when you heard a private sensitive conversation. Your curiosity kicks in so you start listening. They must be far away because the sound is distorted on your end, but when you heard that they were about to reveal a secret pass code you immediately recorded it.

Now can you distinguish what was actually said in the message?!?


[Message link](https://umwctf.ctfd.io/files/176a6961783f0557e44ab2a60b7a0d04/message.mp3)

---

Solution: Load the audio into your favorite audio editor and slow down the audio and change the pitch.
Flag: Fx3TfjH