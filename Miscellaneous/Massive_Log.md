Massive Log File -- Need to Go Deeper
---

**Introduction**

As a totally ethical white-hat hacker who was surely hired to do so legally, you have just broken into a web server. Nice!

Unfortunately however, the account you pwn is not very privileged, so you won't be able to do anything too interesting just yet. Might as well snoop around a bit.

Just then you realize that this is an *Apache* web server. Which means lots of large, juicy log files potentially teeming with sensitive data. And just like many underpaid and overworked website admins, the one for this server left them with improper read permissions. Time to get to work.

**Challenge**

Find the flag in the provided log file:

[Log File](https://ipfs.io/ipfs/QmSoE8YxyRnjNcJHd8paL6PbzAuwBPPrDkg8PNoJ3wd7qJ)

---

Apache `access_log` on line `13943208`:

`dosdude1.com 77.20.212.168 - - [05/Mar/2019:15:35:58 -0500] "GET /gpudisable/ HTTP/1.1" 200 2256 "http://dosdude1.com/resources.html" "galf: 220w9ofa"`

Flag is: `220w9ofa`.

Download link is currently [here](https://ipfs.io/ipfs/QmSoE8YxyRnjNcJHd8paL6PbzAuwBPPrDkg8PNoJ3wd7qJ).