Password Cracker -- Have a Crack at This
---

**Introduction**

You are performing a security audit on a database of users. You do not know this, but the disgruntled database admin is secretly trying to waste your time by giving you access to a small set of usernames and passwords. The passwords are hashed using the DES algorithm, which in it of itself makes you interested in testing the encryption. Your attention then turns to the username *J. Random User* with a password hash of `23k8W5ARWNr1M`.

**Challenge**

Find the plaintext of the given password hash. This will be your flag.

---

The flag in this case is to produce the password in plaintext which is `GO_AWAY`.

Users are expected to produce a C++ program using g++ and the `crypt()` system call, which will accept a C-Style string representing a salt, and a C-Style string that represents the password. They will then input the password into a box in the CTF site.

**Footnote**

To mitigate attempts to get a quick answer without writing the program, we could have users submit a file which will dynamically check with the a dummy flag before checking with a real flag.