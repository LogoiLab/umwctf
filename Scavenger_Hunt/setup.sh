#!/bin/bash

#This script assumes you have IPFS installed and that it is running on the machine you would like to use for hosting.

echo "please insure you have IPFS installed and that you follow all directions provided by this script. If you mess up at any point you will have to restart the process. press [enter] to continue..."

function pause(){
   read -p "$*"
}

ipfs add ./end.html
pause "please copy the above hash and add it to step5.html in the variable HASH press [enter] when done..."
ipfs add ./step5.html
pause "please copy the above hash and add it to step4.html in the variable HASH press [enter] when done..."
ipfs add ./step4.html
pause "please copy the above hash and add it to step3.html in the variable HASH press [enter] when done..."
ipfs add ./step3.html
pause "please copy the above hash and add it to flag.txt in the folder step2_support_files, upload to pastebin then press [enter] when done..."
ipfs add ./step2.html
pause "please copy the above hash and add it to step1.html in the variable HASH press [enter] when done..."
ipfs add ./step1.html
pause "please copy the above hash and add it to start.html in the variable HASH press [enter] when done..."
ipfs add ./start.html
pause "please copy the above hash and add it to the challenge description on ctfd as an ipfs link press [enter] when done..."
